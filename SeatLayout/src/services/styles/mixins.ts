import {Dimensions, PixelRatio} from "react-native";
import {Platform} from "react-native";
import {moderateScale} from "react-native-size-matters";

export const WINDOW_WIDTH = Dimensions.get("window").width;
export const WINDOW_HEIGHT = Dimensions.get("window").height;
const guidelineBaseWidth = 375;
const guidelineBaseHeight = 680;

export const scaleSize = (size) => (WINDOW_WIDTH / guidelineBaseWidth) * size;
export const verticalScale = (size) =>
  (WINDOW_HEIGHT / guidelineBaseHeight) * size;

export const scaleFont = (size) => size * PixelRatio.getFontScale();

//FOR MARGIN AND PADDING
export const appDimensions = {
  XXXLARGE: scaleSize(64),
  XXLARGE: scaleSize(32),
  XLARGE: scaleSize(24),
  LARGE: scaleSize(16),
  NORMAL: scaleSize(8),
  SMALL: scaleSize(4),
  XSMALL: scaleSize(2),
  XXSMALL: scaleSize(1),
};

export const getRemainingWidth = (width) =>
  scaleSize(WINDOW_WIDTH) - scaleSize(width);

//FOR DRAWER
export const DRAWER_WIDTH =
  Platform.OS == "ios" ? scaleSize(250) : scaleSize(170);
export const DRAWER_ITEM_HEIGHT =
  Platform.OS == "ios" ? scaleSize(40) : scaleSize(35);
export const DRAWER_ICON_SIZE =
  Platform.OS == "ios" ? moderateScale(45) : moderateScale(45);

//For Custmo Text Input
export const ET_CONTAINER_HEIGHT =
  Platform.OS == "ios" ? moderateScale(50) : moderateScale(50);
export const ET_CONTAINER_WIDTH =
  Platform.OS == "ios" ? moderateScale(200) : moderateScale(200);
export const ET_ICON_CONTAINER_SIZE =
  Platform.OS == "ios" ? moderateScale(60) : moderateScale(60);
export const ET_ICON_SIZE =
  Platform.OS == "ios" ? moderateScale(40) : moderateScale(45);

//For Custom ToolBar
export const TOOLBAR_HEIGHT = Platform.OS === "ios" ? 44 : 56;
export const TOOLBAR_ICON_HEIGHT = Platform.OS === "ios" ? 25 : 30;
export const TOOLBAR_LOGO_HEIGHT = Platform.OS === "ios" ? 40 : 50;

//For Custom Progress Dialog
export const PROGRESS_DIALOG_ICON_SIZE =
  Platform.OS == "ios" ? scaleSize(60) : scaleSize(60);

//For Snack bar
export const SNACK_HEIGHT =
  Platform.OS == "ios" ? scaleSize(90) : scaleSize(50);
export const SNACK_ICON_SIZE =
  Platform.OS == "ios" ? scaleSize(60) : scaleSize(40);
export const SNACK_ACTION_SIZE =
  Platform.OS == "ios" ? scaleSize(80) : scaleSize(80);

export function boxShadow(
  color,
  offset = {height: 2, width: 2},
  radius = 2,
  opacity = 0.5,
) {
  return {
    shadowColor: color,
    shadowOffset: offset,
    shadowOpacity: opacity,
    shadowRadius: radius,
    elevation: radius,
  };
}
