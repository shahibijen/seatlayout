export const PRIMARY = "#030303";
export const PRIMARY_DARK = "#030303";
export const PRIMARY_TEXT = "#050505";
export const SECONDARY_TEXT = "#363636";
export const ACCENT = "#0693A7";
export const TRANSPARENT = "rgba(0,0,0,0)";
export const SEMI_TRANSPARENT = "rgba(0,0,0,0.7)";
export const SEMI_TRANSPARENT_WHITE = "rgba(255,255,255,0.3)";
export const PROGRESS_BG = "rgba(0,0,0,0.8)";
export const QFX_BACKGROUND_DARK = "#333334";

export const BODY = "#ABAAAB";
export const UTIL = "#828282";
export const ICON_BG = "#EBE8E9";
export const TEXT = "#757575";
export const TIME_TEXT = "#222222";
// export const MENU_ITEM = '#7e7e7e';
export const MENU_ITEM = "#32323280";
export const INPUT_FIELD_BG = "#CDCACC";
export const TOOLTIP_BG = "rgba(189, 187, 188, 0.6)";

export const SMOKE_WHITE = "#f5f5f5";
export const WHITE = "#FFFFFF";
export const BLACK = "#000000";
export const BLACK_LIGHT_GRAY = "#79777743";

//Color value for seat layout
export const DISABLE = "#A8B4BA";
export const BLACK_GRAY = "#282828";
export const ORANGE = "#FF6600";
export const QFX_Q = "#DC7B2E";
export const GRAY_LIGHT = "#e6e6e6";
export const GRAY_MEDIUM = "#cacaca";
export const GRAY_DARK = "#8a8a8a";

{
  /* <color name="light_grey">#E0E0E0</color>
<color name="dark_grey">#FF424242</color> */
}

//Color value for show time indicator circle
export const AVAILABLE = "#09B5A0";
export const FILLING_FAST = "#BA8A1B";
export const YOUR_SEAT = "#FFEB3B";
export const UNAVAILABLE = "#050505";
export const RESERVED = "#2196F3";
export const SOLD_OUT = "#CC2030";
export const SHOW_TIME_BG = "#D8D5D585";

//Color for Goji
export const GOJI_BG = "#26AAD3";
export const GOJI_BUTTON = "#ECAE6B";
export const GOJI_HEADER = "#7BCCD6";

// ACTIONS
export const SUCCESS = "#3adb76";
export const WARNING = "#ffae00";
export const ALERT = "#cc4b37";

//Snackbar Colors
export const SUCCESS_GREEN = "#75D00F";
export const INFO_BLUE = "#3CC3F3";
export const WARNING_YELLOW = "#FFB70A";
export const ERROR_RED = "#EE3B4D";
export const NORMAL_BLACK = "#030303";

//Social Login
export const FACEBOOK_BG = "#303E8B";
export const GOOGLE_BG = "#E0402F";
