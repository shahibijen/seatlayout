import moment from 'moment';
import {constants} from './../constants/constants';

export function getStandardDate(date = moment()) {
  return moment(date).format(dateFormats.YYYY_MM_DD_T_HH_MM_SS_DASH);
}

export function getStandardDisplayDate(date) {
  return moment(date).format(dateFormats.year_month_day_dash);
}

export function getRequiredDate(
  date = moment(),
  format = dateFormats.YYYY_MM_DD_T_HH_MM_SS_DASH,
  currentFormat = dateFormats.YYYY_MM_DD_T_HH_MM_SS_DASH,
) {
  return moment(date, currentFormat).format(format);
}

export function getDayOfWeek(
  date = moment(),
  isTodayTomorrow = false,
  currentFormat = dateFormats.YYYY_MM_DD_T_HH_MM_SS_DASH,
) {
  var checkDate = moment(date, dateFormats.YYYY_MM_DD_T_HH_MM_SS_DASH);
  if (isTodayTomorrow) {
    var REFERENCE = moment();
    var TODAY = REFERENCE.clone().startOf('day');
    var TOMORROW = REFERENCE.clone().add(1, 'days').startOf('day');

    if (checkDate.isSame(TODAY, 'd')) return constants.TODAY;
    else if (checkDate.isSame(TOMORROW, 'd')) return constants.TOMORROW;
    else
      return getRequiredDate(checkDate, dateFormats.day_of_week, currentFormat);
  } else
    return getRequiredDate(checkDate, dateFormats.day_of_week, currentFormat);
}

export function addDaysToDate(
  date = moment(),
  day = 1,
  format = dateFormats.YYYY_MM_DD_T_HH_MM_SS_DASH,
) {
  return moment(date, format).add(day, 'days');
}

export function getMinSecFromSecond(seconds, tagMsg) {
  const min = Math.floor(seconds / 60);
  const sec = seconds % 60;
  return `${min}:${sec} ${tagMsg && tagMsg}`;
}

export function getHrsMinFromMinutes(minutes) {
  const hrs = Math.floor(minutes / 60);
  const mins = minutes % 60;
  return `${hrs} Hrs ${mins} Mins`;
}

export function isWeekEnd(
  date = moment(),
  currentFormat = dateFormats.YYYY_MM_DD_T_HH_MM_SS_DASH,
) {
  var date = moment(date, currentFormat);
  return date.day() === 6;
}

export const dateFormats = {
  month_number: 'MMM',
  year_number: 'YYYY',
  month_day_of_month: 'MMM DD',
  year_month_day_dash: 'YYYY-MM-DD',
  year_month_day_slash: 'YYYY/MM/DD',
  YYYY_MM_DD_T_HH_MM_SS_DASH: "YYYY-MM-DD'T'HH:mm:ss",
  day_of_week: 'dddd',
  hour_min_24_hr: 'HH:mm',
  hour_min_12_hr: 'hh:mm', //08:23  12 Hrs format
  hour_min_12_hr_meridian: 'hh:mm a', //08:23 12 Hrs format with AM
  min_sec: 'mm:ss',
  ddd_HH_MM_SS_DD_MM_YYYY_SLASH: 'ddd hh:mm DD/MM/YYYY',
  DD_MM_YYYY_dddd_SLASH: 'dd/MM/yyyy, dddd',
  ddd_DD_MMM_SPACE: 'ddd DD MMM',
  MMM_D_COMMA_YYYY: 'MMM D, YYYY',
  MM_TEXT_DD_NUMBER_YYYY_NUMBER_HH_MM_12HRS_AM_PM: 'MMM d, yyyy hh:mm a',
};
