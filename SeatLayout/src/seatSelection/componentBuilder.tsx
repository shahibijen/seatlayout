import React from 'react';
import {Text, View, StyleSheet, PixelRatio, Platform} from 'react-native';
import {SeatStatus} from './../constants/constants';
import {appDimensions, Colors, scaleSize} from './../services/styles';

import {moderateScale} from 'react-native-size-matters';

export function seatIndicator(item) {
  return (
    <View style={styles.indicatorContainer}>
      <View style={[styles.indicatorBox, {backgroundColor: item.color}]} />
      <Text style={styles.indicatorTitle}>{item.title}</Text>
    </View>
  );
}
export function SeatItem({
  seat,
  onClick,
  disable,
  buttonStyle,
  buttonTextStyle,
}) {
  return (
    <Text
      onPress={onClick}
      disabled={disable}
      style={{
        ...styles.seat,
        ...buttonTextStyle,
        backgroundColor: getSeatBgColor(seat.ShowSeatStatus),
        ...styles.seatContainer,
        ...buttonStyle,
      }}>
      {seat.ShowSeatStatus === SeatStatus.EMPTY ? '' : seat.SeatLabel}
    </Text>
  );
}

function getSeatBgColor(status) {
  switch (status) {
    case SeatStatus.AVAILABLE:
      return Colors.AVAILABLE;
    case SeatStatus.UNAVAILABLE:
      return Colors.UNAVAILABLE;
    case SeatStatus.RESERVED:
      return Colors.RESERVED;
    case SeatStatus.SOLD:
      return Colors.SOLD_OUT;
    case SeatStatus.YOURSEAT:
      return Colors.YOUR_SEAT;
    case SeatStatus.INFO:
      return Colors.TRANSPARENT;
    case SeatStatus.EMPTY:
      return Colors.TRANSPARENT;
    default:
      return Colors.AVAILABLE;
  }
}

export const styles = StyleSheet.create({
  indicatorContainer: {
    flexDirection: 'row',
    // alignItems: "center",
    // justifyContent: "center",
    padding: moderateScale(4),
  },
  indicatorBox: {
    height: scaleSize(12),
    width: scaleSize(12),
  },
  indicatorTitle: {
    // ...FONT_XXSMALL,
    paddingHorizontal: appDimensions.XSMALL,
    fontSize: 10,
    color: '#000',
  },
  seatContainer: {
    width: PixelRatio.roundToNearestPixel(12),
    height: PixelRatio.roundToNearestPixel(12),
    margin: PixelRatio.roundToNearestPixel(Platform.OS === 'android' ? 1 : 1.8),
    paddingTop: moderateScale(0.4),
    justifyContent: 'center',
    alignItems: 'center',
  },
  seat: {
    // ...FONT_BOLD,
    textAlign: 'center',
    color: Colors.WHITE,
  },
});
