import React, {useCallback, useRef, useState} from 'react';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
import {constants, SeatStatus} from '../constants/constants';
import {
  appDimensions,
  Colors,
  FONT_BOLD,
  FONT_REGULAR,
  FONT_XXSMALL,
  scaleSize,
} from '../services/styles';
import {seatIndicator} from './componentBuilder';

// import {blankTicketIcon, bodyBg} from 'assets/images';

import {
  dateFormats,
  getMinSecFromSecond,
  getRequiredDate,
  isAndroid,
} from '../utils';

import {SeatLayout} from './seatLayout';

const indicators = [
  {
    color: Colors.AVAILABLE,
    title: constants.AVAILABLE,
  },
  {
    color: Colors.RESERVED,
    title: constants.RESERVED,
  },
  {
    color: Colors.SOLD_OUT,
    title: constants.SOLD_OUT,
  },
  {
    color: Colors.YOUR_SEAT,
    title: constants.YOUR_SEAT,
  },
  {
    color: Colors.UNAVAILABLE,
    title: constants.UNAVAILABLE,
  },
];

const operations = {
  RESERVE_SEATS: 'ReserveSeats',
  CANCEL_SEATS: 'CancelSeats',
  PHONE_NOT_VERIFIED: 'PhoneNotVerified',
};
const initialPosition = {
  left: 0,
  top: 0,
  transform: [{scale: 1}],
};

const initialAlertState = {
  message: '',
  operation: '',
  visibility: false,
};

const SeatSelectionScreen = () => {
  const [impState, setImpState] = useState({
    hasImpMsg: false,
    impMessage: '',
  });
  const [timerState, setTimerState] = useState({
    hasTimer: false,
    timerMsg: '',
  });
  const [timerValue, setTimerValue] = useState(0);

  const [seatStatus, setSeatStatus] = useState({
    isSellable: true,
    isReservable: true,
  });

  const [seatCostAndCount, setSeatCostAndCount] = useState({
    seatCount: 0,
    totalCost: 0,
  });

  const [seat, setSeat] = useState({
    seatRows: [
      [
        {
          id: 1,
          ShowSeatStatus: 1,
        },
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        0,
      ],
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
    ],
  });

  const {isSellable, isReservable} = seatStatus;
  const {seatRows} = seat;
  // const {seatParams} = useRoute().params;
  const [loading, setLoading] = useState(false);
  const [seatParams, setParams] = useState({eventName: 'Movie'});

  const gesturesRef = useRef<any>();

  const onSeatSelected = useCallback(seat => {
    console.log(seat, 'seat');
    isAndroid &&
      gesturesRef.current?.zoomPanel.reset(initialPosition, prevStyles => {});
  }, []);

  const isSeatClicable = useCallback(
    status => status === SeatStatus.AVAILABLE || status === SeatStatus.YOURSEAT,
    [],
  );
  return (

      <View style={styles.container}>
          <SeatLayout
            ref={gesturesRef}
            seatRows={seatRows}
            isSeatClicable={isSeatClicable}
            isReservable={isReservable}
            isSellable={isSellable}
            onSeatSelected={onSeatSelected}
            loading={loading}
            styles={styles.text}
          />
      </View>
  );
};

export default SeatSelectionScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    overflow: 'hidden',
    marginTop: moderateScale(5),
    marginBottom: moderateScale(70),
    alignItems: 'center',
  }
});


