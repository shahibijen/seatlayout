export const constants = {
    AVAILABLE: 'Available',
    FILLING_FAST: 'Filling Fast',
    SOLD_OUT: 'Sold Out',
    CONFIRM_PASSWORD: 'Confirm Password',
    YOUR_SEAT: 'Your Seat',
    RESERVED: 'Reserved',
    BUY: 'Buy',
    RESERVE: 'Reserve',
    CANCEL: 'Cancel',
    UNAVAILABLE: 'Unavailable',
    NO_OF_SEAT: 'No. of Seats:',
    TOTAL_COST_NRS: 'Total Cost NRs.',
    WANT_TO_RESERVE_SEAT: 'Do you really want to reserve these seats?',
    CANCEL_SELECTED_SEAT_ALERT_MSG: 'Clear Shopping Cart Session?',
    TODAY: 'TODAY',
    TOMORROW: 'TOMORROW',
  };
  export const SeatStatus = {
    AVAILABLE: 1,
    UNAVAILABLE: 2,
    RESERVED: 3,
    SOLD: 4,
    YOURSEAT: 5,
    EMPTY: 6,
    INFO: 7,
  };
  